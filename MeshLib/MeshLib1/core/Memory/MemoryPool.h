#pragma once
#include <vector>

/*!
*      \file MemoryPool.h
*      \brief A simple implation of memory pool, relying on std::vector and its index;
*
*      The access and the delete of members of memory pool rely the index, which is a size_t variation.
*
*/
struct _memberType {
	bool deleted = false;
};

template<typename T>
class MemoryPool {
public:
	MemoryPool();
	MemoryPool(size_t preAllocate);

	class CMemberT : public T, public _memberType {};
	
	//Generate a new member of type T and return its index.
	size_t newMember();
	//Transform from members index to its pointer.
	T * getPointer(size_t index);
	//deleted the number 
	bool deleteMember(size_t index);
private:
	std::vector<CMemberT> container;
};