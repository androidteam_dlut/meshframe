#include <algorithm>
#include <iostream>
#include <MeshLib2/core/Mesh/BaseMesh.h>
#include <MeshLib2/core/Mesh/Vertex.h>
#include <MeshLib2/core/Mesh/HalfEdge.h>
#include <MeshLib2/core/Mesh/Edge.h>
#include <MeshLib2/core/Mesh/Face.h>
#include <MeshLib2/core/Mesh/Types.h>
#include <MeshLib2/core/Mesh/Iterators.h>
#include <ctime>
#include <stdio.h>
//#define _CRTDBG_MAP_ALLOC   //并非绝对需要该语句，但如果有该语句，打印出来的是文件名和行数等更加直观的信息
//#define _CRTDBG_MAP_ALLOC_NEW
//#include <stdlib.h>
//#include <crtdbg.h>

using namespace MeshLib;

using std::cout;
using std::endl;

class MyCVertex : public CVertexNRGB {
	int ii;
};
class MyCEdge : public CEdge {
	int iii;
};
class MyCFace : public CFace {
	double ccc;
};
class MyCHalfEdge : public CHalfEdge {
	float a;
};

typedef CBaseMesh<MyCVertex, MyCEdge, MyCFace, MyCHalfEdge> M;
typedef CBaseMesh<CVertex, CEdge, CFace, CHalfEdge> BasicM;
typedef CIterators<M> It;

//std::recursive_mutex lock;

int main(int argc, char ** argv) {

	getchar();
	if (argc != 2) {
		printf("Please give a input .m file.\n");
	}
	double count = 0;
	
	clock_t time1;
	clock_t startTime = clock();

	//omp_set_num_threads(4);
	int numTry = 10;
	M  m;
	m.read_ply(argv[1]);

	//VPropHandle<CPoint> vNormalHandle;
	//VPropHandle<std::string> vNameHandle;
	//VPropHandle<int> vIdxHandle;
	//m.addVProp(vNormalHandle);
	//m.addVProp(vNameHandle);
	//m.addVProp(vIdxHandle);
	//int k = 0;
	//for (M::VPtr pV : m.vertices()) {
	//	CPoint& p = m.getVProp(vNormalHandle, pV);
	//	p = M::vertexNormal(pV);
	//
	//	std::string & str = m.getVProp(vNameHandle, pV);
	//	str = std::to_string(k);
	//
	//	int & idx = m.getVProp(vIdxHandle, pV);
	//	idx = k++;
	//	//::cout << p << "\n";
	//}
	//m.removeProp(vNormalHandle);

	//BasicM::Ptr pM = (BasicM::Ptr)&m;
	//for (BasicM::VPtr pV : pM->vertices()) {
	//	//CPoint& p = m.getVProp(vNormalHandle, pV);
	//	//p = M::vertexNormal(pV);
	//
	//	std::string & str = pM->gVP(vNameHandle, pV);
	//	std::cout << str << " - " << pM->getVProp(vIdxHandle, pV) << "\n";
	//	//::cout << p << "\n";
	//}
	//m.write_m("out.m");
	//delete pM;

	//m.addVProp<PropDemo>();
	//printf("PropId change to: %d\n", PropDemo::_propIdx());
	//for (M::VPtr pV : m.vertices()) {
	//	PropDemo& p = m.getVProp<PropDemo>(pV);
	//	assert(p.a + p.b == 0);
	//	//printf("a = %d  b = %d\n", p.a, p.b);
	//	p.a = -2;
	//}
	//for (M::VPtr pV : m.vertices()) {
	//	PropDemo& p = m.getVProp<PropDemo>(pV);
	//	//assert(p.a + p.b == 0);
	//	printf("a = %d  b = %d\n", p.a, p.b);
	//	p.a = -1;
	//}

	
	cout << "Iterating Edges." << endl;
	for (auto pE : It::MEIterator(&m)) {
		M::VPtr pV1, pV2;
		pV1 = M::edgeVertex1(pE);
		pV2 = M::edgeVertex2(pE);
	
		//cout << "The edge's length:" << M::edgeLength(pE);
		//
		//cout << "Vertex 1: " << pV1->point() << endl;
		//cout << "Vertex 2: " << pV2->point() << endl;
	}
	
	for (auto pF : It::MFIterator(&m)) {
		//cout << "The face's size:" << M::faceArea(pF) << "\n";
		//cout << "It's vertices: \n";
		for (auto pV : It::FVIterator(pF)) {
			//cout << pV->index() << " ";
			//cout << "V's Position: " << pV->point() << "\n";
		}
		//cout << "It's edges: \n";
	
		for (auto pE : It::FEIterator(pF)) {
			M::VPtr pV1, pV2;
			pV1 = M::edgeVertex1(pE);
			pV2 = M::edgeVertex2(pE);
	
			//cout << "The edge's length:\n" << M::edgeLength(pE);
	
			//cout << "Vertex 1 id: " << pV1->index() << " " << pV1->point() << endl;
			//cout << "Vertex 2 id: " <<  pV2->index() << " "  << pV2->point() << endl;
		}
	
		for (auto pHF : It::FHEIterator(pF)) {
			//cout << "HalfEdge: " << M::halfedgeVec(pHF) << "\n";
		}
 	}

	for (auto pV : It::MVIterator(&m)) {
		//cout << "Vertex id: " << pV->index() << "\n";
		//cout << "  Number of out HEs: " << pV->outHEs().size() << "\n";
		int numNeiV = 0;
		for (auto pVNei : It::VVIterator(pV)) {
			//cout << "  Nei Vertex id: " << pVNei->index() << "\n";
			++numNeiV;
		}
		if (M::isBoundary(pV)) {
			assert(numNeiV == pV->outHEs().size() + 1);
		}
		else
		{
			assert(numNeiV == pV->outHEs().size());
		}
	
		int numNeiE = 0;
		for (auto pE : It::VEIterator(pV))
		{
			++numNeiE;
		}
		if (M::isBoundary(pV)) {
			assert(numNeiE == pV->outHEs().size() + 1);
		}
		else
		{
			assert(numNeiE == pV->outHEs().size());
		}
	
		int numNeiF = 0;
		for (auto pNeiF : It::VFIterator(pV)) {
			++numNeiF;
		}
		assert(numNeiF == pV->outHEs().size());
		
		int numNeiECcw = 0;
		for (auto pNeiE : It::VCcwEIterator(pV)) {
			++numNeiECcw;
		}
		assert(numNeiECcw == numNeiE);
	
		int numNeiEClw = 0;
		for (auto pNeiE : It::VClwEIterator(pV)) {
			++numNeiEClw;
		}
		assert(numNeiEClw == numNeiE);
	
		int numNeiVCcw = 0;
		for (auto pNeiV : It::VCcwVIterator(pV)) {
			++numNeiVCcw;
		}
		assert(numNeiVCcw == numNeiV);
	
		int numNeiVClw = 0;
		for (auto pNeiV : It::VClwVIterator(pV)) {
			++numNeiVClw;
		}
		assert(numNeiVClw == numNeiV);
	
		int numNeiHEs = 0;
		It::VOutHEIterator voutheIter(pV);
		for (auto pHE : voutheIter) {
			++numNeiHEs;
		}
		assert(numNeiHEs == pV->outHEs().size());
	
		int numCcwNeiHEs = 0;
		for (M::HEPtr pHE : It::VCcwOutHEIterator(pV)) {
			++numCcwNeiHEs;
		}
		assert(numCcwNeiHEs == numNeiHEs);
	
		int numClwInHEs = 0;
		for (M::HEPtr pHE : It::VClwInHEIterator(pV)) {
			++numClwInHEs;
		}
		assert(numClwInHEs == numNeiHEs);
	}

	
	//getchar();
}
	