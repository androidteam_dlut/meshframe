#include <algorithm>
#include <iostream>
#include <MeshLib/core/Mesh/BaseMesh.h>
#include <MeshLib/core/Mesh/Vertex.h>
#include <MeshLib/core/Mesh/HalfEdge.h>
#include <MeshLib/core/Mesh/Edge.h>
#include <MeshLib/core/Mesh/Face.h>


using namespace MeshLib;

using std::cout;
using std::endl;

typedef CBaseMesh<CVertex, CEdge, CFace, CHalfEdge> M;



int main() {
	float pointsBuf[] = { 1.0, 0 ,0, 1.0, 0 ,0 , 1.0, 0 ,0 };
	short facesBuf[] = { 0 , 1, 2 };

	M * pM = new M;
	pM->read_buffer(pointsBuf, 3, facesBuf, 1);

	getchar();
}
	