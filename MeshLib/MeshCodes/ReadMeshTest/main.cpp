#include <algorithm>
#include <iostream>
#include <MeshLib/core/Mesh/BaseMesh.h>
#include <MeshLib/core/Mesh/Vertex.h>
#include <MeshLib/core/Mesh/HalfEdge.h>
#include <MeshLib/core/Mesh/Edge.h>
#include <MeshLib/core/Mesh/Face.h>
#include <MeshLib/core/Mesh/Types.h>
#include <MeshLib/core/Mesh/Iterators2.h>
#include <ctime>
#include <stdio.h>
#define _CRTDBG_MAP_ALLOC   //并非绝对需要该语句，但如果有该语句，打印出来的是文件名和行数等更加直观的信息
#define _CRTDBG_MAP_ALLOC_NEW
#include <stdlib.h>
#include <crtdbg.h>

using namespace MeshLib;

using std::cout;
using std::endl;

typedef CBaseMesh<CVertexNUV, CEdge, CFace, CHalfEdge> M;
typedef CIterators<M> It;

#define REPEAT_TIMES 2

//std::recursive_mutex lock;

int main(int argc, char ** argv) {


	int * d = new int[10];
	clock_t timeReadBinaryPly = clock();
	for (size_t i = 0; i < REPEAT_TIMES; i++)
	{
		M m;
		m.read_obj(argv[1]);

		//m.write_m("dragon.m");
		//m.read_obj(argv[1]);
	}
	timeReadBinaryPly = clock() - timeReadBinaryPly;
	printf("Time consumption in read obj: %d\n", timeReadBinaryPly / REPEAT_TIMES);
	//clock_t timeReadASCPly = clock();
	//for (size_t i = 0; i < REPEAT_TIMES; i++)
	//{
	//	M m;
	//	m.read_ply(argv[2]);
	//}
	//timeReadASCPly = clock() - timeReadASCPly;
	//printf("Time consumption in read ply ASCII: %d\n", timeReadASCPly / REPEAT_TIMES);
	//
	//
	//
	//_CrtDumpMemoryLeaks();
	//
	//clock_t timeReadObj = clock();
	//for (size_t i = 0; i < REPEAT_TIMES; i++)
	//{
	//	M m;
	//	m.read_obj(argv[3]);
	//}
	//timeReadObj = clock() - timeReadObj;
	//printf("Time consumption in read obj: %d\n", timeReadObj / REPEAT_TIMES);
	getchar();
}
	