#include <MeshLib\core\Mesh\MeshCoreHeaders.h>

#include <string>
#include <vector>
#include <stdio.h>

#include <ctime>
#include <random>
#define CRTDBG_MAP_ALLOC 
#include <stdlib.h>    
#include <crtdbg.h>  

#define NUM_MEMBERS 2000
#define NUM_TO_D
#define NUM_TO_DELETE 200
#define NUM_REPEAT 10000000

std::default_random_engine generator(time(NULL));
std::uniform_int_distribution<int> randInt(0, NUM_TO_DELETE);

using namespace MeshLib;

class MyV : public CVertex {
public:
	CPoint m_normal;
};

class MyF : public CFace {
public:
	CPoint m_normal;

};

class MyE : public CEdge {
	double weight;
};

class MyHE : public CHalfEdge {
	double a;
};

typedef CBaseMesh<MyV, MyE, MyF, MyHE> M;
typedef CIterators<M> It;


int main(int argc, char ** argv) {

	//std::string * pStrs = new std::string[100]{"Demo"};
	//for (int i = 0; i < 100; i++)
	//{
	//	std::cout << pStrs[i] << std::endl;
	//}
	//int * pInt = new int(100);
	//PropPool<int> * pIntPool = new PropPool<int>(100);
	//for (int i = 0; i < 10000; i++)
	//{
	//	(*pIntPool)[i] = i;
	//	printf("%d\n", (*pIntPool)[i]);
	//}
	//delete pIntPool;
	//_CrtDumpMemoryLeaks();
	//getchar();
	if (argc < 2) {
		printf("Need a input .m file! \n");
		return -1;
	}
	{
		M m;
		VPropHandle<int> vIntHdl;
		VPropHandle<int> vIntHdl2;
		VPropHandle<std::string> vStrHdl;
		m.addVProp(vStrHdl, std::string("demo"));
		m.addVProp(vIntHdl, 123);
		m.read_obj(argv[1]);
		m.addVProp(vIntHdl2, 1233);

		for (M::VPtr pV : m.vertices())
		{
			//m.gVP(vIntHdl, pV) = 111;
			printf("%d\n", m.gVP(vIntHdl, pV));
			printf("%d\n", m.gVP(vIntHdl2, pV));
			printf("%s\n", m.gVP(vStrHdl, pV).c_str());
		}
		m.removeVProp(vIntHdl);
	}
	_CrtDumpMemoryLeaks();
	getchar();
	//clock_t time1 = clock();
	////for (int i = 0; i < 100; i++)
	////{
	////	for (M::VPtr pV : It::MVIterator(&m))
	////	{
	////		//pV->m_normal = CPoint();
	////		for (M::FPtr pF : It::VFIterator(pV)) {
	////			pF->m_normal = M::faceNormal(pF);
	////		}
	////	}
	////}
	////time1 = clock() - time1;
	////printf("Time consumption %dms\n", time1);
	//int a = int();
	//
	//time1 = clock();
	//const int repeatTime = 5;
	//for (int i = 0; i < repeatTime; i++)
	//{
	//#pragma omp parallel for num_threads(4)
	//	for (int i = 0; i < m.vertices().getCurrentIndex(); ++i)//It::MVIterator(&m))
	//	{
	//		double totalArea = 0;
	//		M::VPtr pV = &m.vertices()[i];
	//		pV->m_normal = CPoint();
	//		for (M::FPtr pF : It::VFIterator(pV)) {
	//			pF->m_normal = M::faceNormal(pF);
	//			double area = M::faceArea(pF);
	//			pV->m_normal += pF->m_normal * area;
	//			totalArea += area;
	//		}
	//		pV->m_normal /= totalArea;
	//	}
	//}
	//time1 = clock() - time1;
	//printf("Time consumption %dms\n", time1 / repeatTime);
	//
	//getchar();
}
