#include <MeshLib/core/Memory/MemoryPool.h>
#include <MeshLib/core/Memory/MPIterator.h>
#include <string>
#include <vector>
#include <stdio.h>

#include <ctime>
#include <random>
#define NUM_MEMBERS 2000
#define NUM_TO_D
#define NUM_TO_DELETE 200
#define NUM_REPEAT 10000000

std::default_random_engine generator(time(NULL));
std::uniform_int_distribution<int> randInt(0, NUM_TO_DELETE);

class DemoClass {
public:
	DemoClass() :
		str("Demo.")
	{
		i = 100;
	}
	std::string getStr() { return str; };
private:
	int i;
	char str[100];
};

typedef MemoryPool<DemoClass> CClassContainer;

int main(int argc, char ** argv) {
	CClassContainer mp;
	std::vector<DemoClass *> ptrs1(NUM_REPEAT);
	std::vector<DemoClass *> ptrs2(NUM_REPEAT);

	clock_t time1 = clock();

	for (int i = 0; i < NUM_REPEAT; ++i) {
		ptrs1[i] = new DemoClass;
	}
	time1 = clock() - time1;

	clock_t time2 = clock();
	size_t id;
	for (int i = 0; i < NUM_REPEAT; ++i) {
		ptrs2[i] = mp.newMember(id);
	}
	time2 = clock() - time2;

	printf("Time consumption in new: %d\n", time1);
	printf("Time consumption in mp: %d\n", time2);

	getchar();
}
