//#include <MeshLib\core\Mesh\MeshCoreHeaders.h>
#include <MeshLib\core\Mesh\BaseMesh.h>
#include <MeshLib\core\Mesh\Vertex.h>
#include <MeshLib\core\Mesh\Edge.h>
#include <MeshLib\core\Mesh\Face.h>
#include <MeshLib\core\Mesh\HalfEdge.h>

#include <MeshLib\core\Mesh\iterators.h>
#include <string>
#include <vector>
#include <stdio.h>

#include <ctime>
#include <random>
#define NUM_MEMBERS 2000
#define NUM_TO_D
#define NUM_TO_DELETE 200
#define NUM_REPEAT 10000000

std::default_random_engine generator(time(NULL));
std::uniform_int_distribution<int> randInt(0, NUM_TO_DELETE);

using namespace MeshLib;

class MyV : public CVertex {
	CPoint m_normal;
};

class MyF : public CFace {
public:
	CPoint m_normal;

};

class MyE : public CEdge {
	double weight;
};

class MyHE : public CHalfEdge {
	double a;
};

typedef CBaseMesh<MyV, MyE, MyF, MyHE> M;
typedef MeshVertexIterator<MyV, MyE, MyF, MyHE> MVIter;
typedef MeshFaceIterator<MyV, MyE, MyF, MyHE> MFIter;
typedef MeshEdgeIterator<MyV, MyE, MyF, MyHE> MEIter;
typedef MeshHalfEdgeIterator<MyV, MyE, MyF, MyHE> MHEIter;
typedef VertexFaceIterator<MyV, MyE, MyF, MyHE> VFIter;

//C:\Code\Android\BitBucketTeam\AndroidGeometry\MeshLib
int main(int argc, char ** argv) {
	if (argc < 2) {
		printf("Need a input .m file! \n");
		return -1;
	}
	
	M m;
	m.read_m(argv[1]);

	clock_t time1 = clock();

	for (int i = 0; i < 100; i++)
	{
		for (MVIter iter(&m); !iter.end(); ++iter)
		{
			MyV * pV = *iter;
			for (VFIter vfIter(pV); !vfIter.end(); ++vfIter)
			{
				CPoint v1, v2;
				MyF * pF = *vfIter;
				MyHE * pHE = m.faceHalfedge(pF);
				v1 = pHE->target()->point() - pHE->source()->point();
				pHE = m.halfedgeNext(pHE);
				v2 = pHE->target()->point() - pHE->source()->point();
				pF->m_normal = v1^v2;
				pF->m_normal /= pF->m_normal.norm();
			}
		}
	}
	time1 = clock() - time1;
	printf("Time Consumption: %d\n", time1);

	for (MFIter fIter(&m); !fIter.end(); ++fIter)
	{
		MyF * pV = *fIter;
	}

	for (MEIter eIter(&m); !eIter.end(); ++eIter) 
	{
		MyE * pE = *eIter;
	}
	for (MHEIter heIter(&m); !heIter.end(); ++heIter)
	{
		MyHE * pHE = *heIter;
	}

	getchar();
}
