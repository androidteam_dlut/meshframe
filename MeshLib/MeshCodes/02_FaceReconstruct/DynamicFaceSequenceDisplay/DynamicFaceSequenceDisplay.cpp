#include <MeshLib/core/Mesh/MeshCoreHeaders.h>
#include <MeshLib/core/viewer/MeshViewer.h>
#include <AC/AC.h>
#include <AC/Def.h>
#include <AC/IO_AC.h>
#include <ctime>

typedef MeshLib::CBaseMesh<MeshLib::CVertexUV, MeshLib::CEdge, MeshLib::CFace, MeshLib::CHalfEdge> M;
typedef MeshLib::CIterators<M> It;

MeshLib::CMeshViewer * pViewer;
AC::VecStr filesObj;
AC::VecStr filesBmp;
M * pMesh = NULL;
clock_t waitTimeStart;
MeshLib::CPoint centroid;

bool startPlay = true;

float scale;

const int totalWaitTimeThres = 100;




void getMeshScale(M * pM, MeshLib::CPoint & c, float & s) {
	MeshLib::CPoint center(0, 0, 0);
	MeshLib::CPoint min(0, 0, 0);
	MeshLib::CPoint max(0, 0, 0);

	for (auto pV : It::MVIterator((M::Ptr)pM)) {
		MeshLib::CPoint v = pV->point();
		center += v;
		for (int i = 0; i < 3; ++i) {
			min[i] = v[i] < min[i] ? v[i] : min[i];
			max[i] = v[i] > max[i] ? v[i] : max[i];
		}
	}
	center = center / pM->vertices().size();
	centroid = center;
	MeshLib::CPoint scl = max - min;
	s = (scl[0] + scl[1] + scl[2]) / 3;
	if (s == 0.0)
	{
		s = 1.0;
	}
}

void normalizeMesh(M * pM, MeshLib::CPoint c, float s) {
	for (auto pV : It::MVIterator(pM)) {
		pV->point() = (pV->point() - c) / s;
	}
}

void switchFaceIdleFunc() {

	//if (!startPlay)
	//{
	//	return;
	//}

	int size = filesObj.size();
	static int i = 0;
	int totalWaitTime = ((float)(clock() - waitTimeStart) * 1000) / CLOCKS_PER_SEC;
	if (totalWaitTime < totalWaitTimeThres)
	{
		return;
	}
	else {
		printf("Frame: %d\n", i);
		waitTimeStart = clock();
		delete pMesh;
		pMesh = new M;
		pMesh->read_obj(filesObj[i].c_str());
		normalizeMesh(pMesh, centroid, scale);
		pViewer->setMeshPointer(pMesh, true, false, true);
		pViewer->setTexture(filesBmp[i].c_str());
		++i;
		if (i >= size)
		{
			i = 0;
		}

	}
}


int main(int argc, char ** argv) {
	if (argc < 2) {
		printf("Need 13 input file: mesh.\n");
		return -1;
	}

	AC::IO::getFilesWithExt(argv[1], "obj", filesObj);
	AC::IO::getFilesWithExt(argv[1], "bmp", filesBmp);

	M m;
	m.read_obj(filesObj.front().c_str());
	waitTimeStart = clock();
	getMeshScale(&m, centroid, scale);
	normalizeMesh(&m, centroid, scale);

	MeshLib::CMeshViewer viewer;
	pViewer = &viewer;
	viewer.setMeshPointer(&m, true, false, true);
	viewer.setTexture(filesBmp.front().c_str());
	viewer.setUserIdleFunc(switchFaceIdleFunc);
	viewer.show();
	delete pMesh;
}