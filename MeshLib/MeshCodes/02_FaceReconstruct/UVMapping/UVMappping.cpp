#include <MeshLib2/core/Mesh/MeshCoreHeaders.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/ply_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/search/flann_search.h>
#define KNN_SEARCH_K 16

#define IMAGE_WIDTH 1380
#define IMAGE_HEIGHT 1160
typedef MeshLib::CBaseMesh<MeshLib::CVertexUV, MeshLib::CEdge, MeshLib::CFace, MeshLib::CHalfEdge> M;
typedef MeshLib::CIterators<M> It;

typedef pcl::PointXYZ BasicPType;
typedef pcl::PointCloud<pcl::PointXYZ> BasicPCType;

int main(int argc, char ** argv) {
	if (argc != 4) {
		printf("Need 3 input file: mesh, cloud, cloudUV.\n");
		return -1;
	}

	M m;
	m.read_ply(argv[1]);

	BasicPCType::Ptr pCloud(new BasicPCType);
	pcl::io::loadPLYFile(argv[2], *pCloud);
	BasicPCType::Ptr pUVCloud(new BasicPCType);
	pcl::io::loadPLYFile(argv[3], *pUVCloud);

	pcl::KdTreeFLANN<BasicPType> kdtree;
	kdtree.setInputCloud(pCloud);

	for (M::VPtr pV : It::MVIterator(&m))
	{
		MeshLib::CPoint p = pV->point();
		BasicPType pp;
		pp.x = p[0];
		pp.y = p[1];
		pp.z = p[2];

		std::vector<int> pointIdxKNNSearch;
		std::vector<float> pointKNNSquaredDistance;

		kdtree.nearestKSearch(pp, KNN_SEARCH_K, pointIdxKNNSearch, pointKNNSquaredDistance);

		MeshLib::CPoint2 uv;
		double totalDistanceReciprocal = 0;
		for (int i = 0; i < KNN_SEARCH_K; ++i) {
			int idx = pointIdxKNNSearch[i];
			MeshLib::CPoint2 uvNei(pUVCloud->points[idx].x / IMAGE_WIDTH, 1- pUVCloud->points[idx].y / IMAGE_HEIGHT);
			double  dReciprocal = 1 / (0.01 + sqrt(pointKNNSquaredDistance[i]));
			//double  dReciprocal = 1;
			uv +=  uvNei * dReciprocal;
			totalDistanceReciprocal = totalDistanceReciprocal + dReciprocal;
		}
		uv /= totalDistanceReciprocal;
		pV->uv() = uv;
		//pV->uv() = MeshLib::CPoint2(pUVCloud->points[pointIdxKNNSearch[0]].x / IMAGE_WIDTH, pUVCloud->points[pointIdxKNNSearch[0]].y / IMAGE_HEIGHT);
	}

	FILE *fp;
	if (fp = fopen("out.obj", "wb"))
		puts("Succeeded to open file.\n");
	else
		puts("Failed to open file.\n");

	fprintf(fp, "mtllib out.mtl\n");
	for (M::VPtr pV : It::MVIterator(&m))
	{
		fprintf(fp, "v %f %f %f \n", pV->point()[0], pV->point()[1], pV->point()[2]);
	}

	for (M::VPtr pV : It::MVIterator(&m))
	{
		fprintf(fp, "vt %f %f \n", pV->uv()[0], pV->uv()[1]);
	}
	
	fprintf(fp, "usemtl FaceTexture\n");
	for (M::FPtr pF : It::MFIterator(&m)) {
		fprintf(fp, "f ");
		for (M::VPtr pV : It::FVIterator(pF)) {
			fprintf(fp, "%d/%d ", pV->index()+1, pV->index()+1);
		}
		fprintf(fp, "\n");

	}
	fclose(fp);
}