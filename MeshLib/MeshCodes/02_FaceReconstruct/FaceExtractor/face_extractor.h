#ifndef _FACE_EXTRATOR_H_
#define _FACE_EXTRATOR_H_

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

typedef pcl::PointXYZRGBNormal PType;
typedef pcl::PointCloud<PType> PCType;
typedef pcl::PointXYZ UVPType;
typedef pcl::PointCloud<UVPType> UVPCType;

struct RowCol{
	RowCol() {};
	RowCol(int inRow, int inCol) : row(inRow), col(inCol) {};
	int row;
	int col;
};
class FaceExtractor;

class FaceExtractorAPI
{
public:
	FaceExtractorAPI(): _face_extractor(nullptr) {};
	~FaceExtractorAPI() 
	{
		if (_face_extractor != nullptr)
			delete _face_extractor;
	};
	/** Function to set up the extractor.
	* \param[in] mesh_path��the path of original mesh
	* \param[in] output_filename
	* \param[in] texture_nr: the number of rows of the texture
	* \param[in] texture_nc: the number of columns of the texture
	* \param[in] dlib_landmarks: the 68 face landmarks dectected by dlib 
	* \param[in] pcloud: the point cloud 
	* \param[in] pcloud_uv: the uv of cloud
	*/
	void SetUpExtractor(const char * mesh_path,
		const char * output_filename,
		const int & texture_nr,
		const int & texture_nc,
		std::vector<RowCol> & dlib_landmarks,
		PCType::Ptr pcloud,
		UVPCType::Ptr pclud_uv);

	/*
	* Function to extract face from original mesh and to output a face mesh with obj format.
	*/
	void ExtractFace();
private:
	bool _set_up_succeed = false;
	FaceExtractor * _face_extractor;
};
#endif
