#ifndef _UV_MESH_H_
#define _UV_MESH_H_
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <deque>

#include <MeshLib/core/Mesh/MeshCoreHeaders.h>
#include <MeshLib/core/Memory/MemoryPool.h>
#include <MeshLib/core/Memory/MPIterator.h>

struct FileParts
{
	std::string path;
	std::string name;
	std::string ext;
};

namespace MeshLib
{
	class uvHalfEdge;
	class uvVertex : public CVertexNUVRGB
	{
	public:
		uvVertex() :
			_is_uv_bad(false), 
			_is_in_face(false),
			_shortest_radius(-1.0) {};
		bool & IsUVBad() { return _is_uv_bad; };
		bool & IsMainComponent() { return _is_main_component; };
		bool & IsInFace() { return _is_in_face; };
		float & ShortestRadius() { return _shortest_radius; };
	protected:
		bool _is_uv_bad;
		bool _is_main_component;
		bool _is_in_face;
		float _shortest_radius;
	};
	class uvEdge : public CEdge
	{
	public:
		bool & sharp() { return _sharp; };
	private:
		bool _sharp;
	};
	class uvFace : public CFaceN
	{
	public:
		uvFace() : _is_in_face(false) {};
		bool & IsMainComponent() { return _is_main_cpmponent; };
		bool & IsInFace() { return _is_in_face; };
	private:
		bool _is_main_cpmponent;
		bool _is_in_face;
	};
	class uvHalfEdge : public CHalfEdge
	{
	public:

	private:

	};

	template<typename V, typename E, typename F, typename H>
	class uvBaseMesh : public CBaseMesh<V, E, F, H>
	{
	public:
		typedef CIterators<uvBaseMesh> MIterators;
		void SetMainComponent();
		int OutputObj(const char * file_name);
		FileParts fileparts(std::string filename);
	};
	typedef uvBaseMesh<uvVertex, uvEdge, uvFace, uvHalfEdge> uvMesh;
	typedef CIterators<uvMesh> uvIterators;

	template<typename V, typename E, typename F, typename H>
	void uvBaseMesh<V, E, F, H>::SetMainComponent()
	{
		FPropHandle<int> component_index_handle;
		addFProp(component_index_handle);

		int index_count = 0, face_count = 0, main_index = -1;
		//initial FProp
		for (F * pface : faces())
		{
			getFProp(component_index_handle, pface) = -1;
		}
		for (F * pface : MIterators::MFIterator(this))
		{
			int & index = getFProp(component_index_handle, pface);
			if (index == -1)
			{
				index = index_count;
				face_count = 1;
				std::queue<F *> face_que;
				face_que.push(pface);
				while (!face_que.empty())
				{
					F * pface0 = face_que.front();
					face_que.pop();
					for (H * phalfedge : MIterators::FHEIterator(pface0))
					{
						if (isBoundary(phalfedge)) continue;
						phalfedge = halfedgeSym(phalfedge);
						F * pface1 = halfedgeFace(phalfedge);
						int & face_index = getFProp(component_index_handle, pface1);
						if (face_index == -1)
						{
							face_index = index_count;
							face_que.push(pface1);
							face_count++;
						}
					}
				}
				if (face_count >= numFaces() / 2.0)
					main_index = index_count;
				face_count = 0;
				index_count++;
			}
		}
		//delete faces
		for (F * pface : MIterators::MFIterator(this))
		{
			if (getFProp(component_index_handle, pface) == main_index)
			{
				pface->IsMainComponent() = true;
			}
			else
			{
				pface->IsMainComponent() = false;
			}
			for (V * pvertex : MIterators::FVIterator(pface))
			{
				pvertex->IsMainComponent() = pface->IsMainComponent();
			}
		}
		removeFProp(component_index_handle);
	}

	template<typename V, typename E, typename F, typename H>
	int uvBaseMesh<V, E, F, H>::OutputObj(const char * file_name)
	{
		FileParts file_parts = fileparts(std::string(file_name));
		std::string outObjName = file_parts.path + file_parts.name + ".obj";
		std::string outMtlName = file_parts.path + file_parts.name + ".mtl";
		VPropHandle<int> writen_id;
		addVProp(writen_id, -1);

		FILE *fp;
		if (!(fp = fopen(outObjName.c_str(), "wb"))) {
			printf("Failed to open file %s.\n", outObjName.c_str());
			return -1;
		}
		fprintf(fp, ("mtllib " + file_parts.name + ".mtl\n").c_str());
		int id_count = 1;
		for (F * pf : MIterators::MFIterator(this))
		{
			if (!pf->IsInFace()) continue;
			for (V * pv : MIterators::FVIterator(pf))
			{
				int & vid = getVProp(writen_id, pv);
				if ( vid != -1) continue;
				vid = id_count++;
				fprintf(fp, "v %f %f %f \n", pv->point()[0], pv->point()[1], pv->point()[2]);
				fprintf(fp, "vt %f %f \n", pv->uv()[0], pv->uv()[1]);
			}
		}

		fprintf(fp, "usemtl FaceTexture\n");
		for (F * pf : MIterators::MFIterator(this)) 
		{
			if (!pf->IsInFace()) continue;
			fprintf(fp, "f ");
			for (V * pv : MIterators::FVIterator(pf)) 
			{
				int vid = getVProp(writen_id, pv);
				fprintf(fp, "%d/%d ", vid, vid);
			}
			fprintf(fp, "\n");

		}
		fclose(fp);
		removeVProp(writen_id);
		if (!(fp = fopen(outMtlName.c_str(), "wb"))) {
			printf("Failed to open file %s.\n", outMtlName.c_str());
			return -1;
		}

		fprintf(fp, "newmtl FaceTexture\n");
		std::string ss = "map_Kd ";
		ss += file_parts.name + ".jpg";
		fprintf(fp, ss.c_str());

		fclose(fp);
		return 0;
	}

	template<typename V, typename E, typename F, typename H>
	FileParts uvBaseMesh<V, E, F, H>::fileparts(std::string filename)
	{
		std::replace(filename.begin(), filename.end(), '\\', '/'); // replace all '\' to '/', fuck Microsophtte

		int idx0 = filename.rfind("/");

		int idx1 = filename.rfind(".");

		FileParts fp;
		fp.path = filename.substr(0, idx0 + 1);
		fp.name = filename.substr(idx0 + 1, idx1 - idx0 - 1);
		if (idx1 != -1) {
			fp.ext = filename.substr(idx1);
		}
		else
		{
			fp.ext = "";
		}
		return fp;
	}
};
#endif
