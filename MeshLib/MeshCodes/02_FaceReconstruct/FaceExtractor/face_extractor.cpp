#include <queue>
#include <pcl/kdtree/kdtree_flann.h>

#include "face_extractor.h"
#include "uv_mesh.h"
using namespace MeshLib;

#define NEAREST_NEIGHBOR_NUM 50

class FaceExtractor
{
public:
	FaceExtractor();
	FaceExtractor(
		const char * mesh_path,
		const char * output_filename,
		const int & texture_nr,
		const int & texture_nc,
		PCType::Ptr pcloud,
		UVPCType::Ptr pclud_uv);
	~FaceExtractor()
	{
		_bottom_boundary_row->swap(std::vector<int>());
		_top_boundary_row->swap(std::vector<int>());
		if (_landmark_vertices != nullptr)
			_landmark_vertices->swap(std::vector<uvVertex *>());
		//if (_pmesh != nullptr)
			delete _pmesh;
	}
	void SetDlibLandmarks(std::vector<RowCol> & dlib_landmarks);
	int ExtractFace();

private:
	void CalculateBoundary();
	void GetRowCol(int & row, int & col, const CPoint2 & uv);
	void SetMeshConponent();
	void CutMeshWithPlane();
	void CalculateKeyVertices();
	void SetupFaces();
	void MakeFaceMeshConnected();

	void ConstructMeshUV();
	UVPType CalculateOneUV(
		pcl::KdTreeFLANN<PType> &kdtree,
		uvVertex * pvertex);
	void FindBadVertices();
	void DoubleAverageValue(float * twovalue, std::vector<float> & dataset);

private:
	int _nr;
	int _nc;
	int _sub_row0;
	int _sub_col0;
	int _sub_nr;
	int _sub_nc;

	uvMesh * _pmesh;
	PCType::Ptr _pcloud;
	UVPCType::Ptr _pcloud_uv;
	const char * _file_name;

	std::vector<RowCol> _dlib_landmarks;
	std::vector<int> * _bottom_boundary_row;
	std::vector<int> * _top_boundary_row;
	std::vector<uvVertex *> * _landmark_vertices;
};

void FaceExtractor::CutMeshWithPlane()
{
	float a[2], b[2], c[2];
	for (int i = 0; i < 2; i++)
	{
		CPoint p0, p1, pp;
		pp = _landmark_vertices->at(i + 4)->point();
		p0 = pp - _landmark_vertices->at(0)->point();
		p1 = pp - _landmark_vertices->at(1)->point();
		p0[1] = pp[1] - _landmark_vertices->at(i + 2)->point()[1];
		p1[1] = pp[1] - _landmark_vertices->at(i + 2)->point()[1];
		float det = p0[0] * p1[1] - p0[1] * p1[0];
		a[i] = -(p1[1] * p0[2] - p0[1] * p1[2]) / det;
		b[i] = -(p1[2] * p0[0] - p0[2] * p1[0]) / det;
		c[i] = a[i] * pp[0] + b[i] * pp[1] + pp[2];
		//std::cout << "Plane a = " << a[i - 2] << " b = " << b[i - 2] << " c = " << c[i - 2] << std::endl;
	}
	for (uvVertex * pv : uvIterators::MVIterator(_pmesh))
	{
		if (!pv->IsMainComponent())
		{
			pv->IsInFace() = false;
			continue;
		}
		CPoint p = pv->point();
		float forehead_c, chin_c;
		forehead_c = a[0] * p[0] + b[0] * p[1] + p[2];
		chin_c = a[1] * p[0] + b[1] * p[1] + p[2];
		if (forehead_c < c[0] 
			&& chin_c < c[1])
		{
			if ((p[0] > _landmark_vertices->at(0)->point()[0]
				&& p[0] < _landmark_vertices->at(1)->point()[0])
				|| !pv->IsUVBad())
			{
				pv->IsInFace() = true;
			}
			else
			{
				pv->IsInFace() = false;
			}
		}
		else
		{
			pv->IsInFace() = false;
		}
	}
}

void FaceExtractor::CalculateKeyVertices()
{
	int row[6], col[6];//right ear, left ear, between eyebrows, nose, forehead, chin
	_landmark_vertices = new std::vector<uvVertex *>(6);
	row[0] = _dlib_landmarks[1].row;
	col[0] = _sub_col0;
	row[1] = _dlib_landmarks[15].row;
	col[1] = _sub_col0 + _sub_nr;
	row[2] = (_dlib_landmarks[17].row + _dlib_landmarks[26].row) / 2;
	col[2] = _dlib_landmarks[27].col;
	row[3] = _dlib_landmarks[33].row;
	col[3] = _dlib_landmarks[33].col;
	int step = 5;
	float dist[4] = { 1.0e10, 1.0e10 , 1.0e10 , 1.0e10 };
	row[4] = _nr;
	row[5] = 0;
	for (uvVertex * pv : uvIterators::MVIterator(_pmesh))
	{
		if (pv->IsUVBad()) continue;
		if (!pv->IsMainComponent()) continue;
		int row0, col0;
		GetRowCol(row0, col0, pv->uv());
		for (int i = 0; i < 4; i++)
		{
			float dist0 = (row0 - row[i]) * (row0 - row[i]) + (col0 - col[i]) * (col0 - col[i]);
			if (dist0 < dist[i])
			{
				_landmark_vertices->at(i) = pv;
				dist[i] = dist0;
			}
		}
		if (col0 < _sub_col0 + _sub_nc / 2 + step
			&& col0 > _sub_col0 + _sub_nc / 2 - step)
		{
			if (row0 < row[4])
			{
				_landmark_vertices->at(4) = pv;
				row[4] = row0;
			}
			if (row0 > row[5])
			{
				_landmark_vertices->at(5) = pv;
				row[5] = row0;
			}
		}
	}
}

void FaceExtractor::SetupFaces()
{
	FPropHandle<bool> face_handle;
	_pmesh->addFProp(face_handle, false);
	for (uvFace * pf : uvIterators::MFIterator(_pmesh))
	{
		if (!pf->IsMainComponent())
		{
			pf->IsInFace() = false;
			continue;
		}
		int count = 0;
		for (uvVertex * pv : uvIterators::FVIterator(pf))
		{
			if (pv->IsInFace())
				count++;
		}
		if (count >= 2)
			pf->IsInFace() = true;
		else
			pf->IsInFace() = false;
	}
	for (uvFace * pf : uvIterators::MFIterator(_pmesh))
	{
		if (!pf->IsInFace()) continue;
		int count = 0;
		for (uvHalfEdge * ph : uvIterators::FHEIterator(pf))
		{
			if (_pmesh->isBoundary(ph)) continue;
			ph = _pmesh->halfedgeSym(ph);
			if (_pmesh->halfedgeFace(ph)->IsInFace())
				count++;
		}
		if (count == 1)
			_pmesh->getFProp(face_handle, pf) = true;
	}
	for (uvFace * pf : uvIterators::MFIterator(_pmesh))
	{
		if (_pmesh->getFProp(face_handle, pf))
			pf->IsInFace() = false;
	}
	_pmesh->removeFProp(face_handle);
}

void FaceExtractor::MakeFaceMeshConnected()
{
	uvFace * current_face = 
		_pmesh->halfedgeFace(_pmesh->vertexHalfedge(_landmark_vertices->at(3)));
	assert(current_face->IsInFace());
	FPropHandle<bool> marked;
	_pmesh->addFProp(marked, false);
	std::queue<uvFace *> face_que;
	face_que.push(current_face);
	while (!face_que.empty())
	{
		current_face = face_que.front();
		face_que.pop();
		if (_pmesh->getFProp(marked, current_face)) continue;
		_pmesh->getFProp(marked, current_face) = true;
		for (uvHalfEdge * ph : uvIterators::FHEIterator(current_face))
		{
			if (_pmesh->isBoundary(ph)) continue;
			ph = _pmesh->halfedgeSym(ph);
			uvFace * pf = _pmesh->halfedgeFace(ph);
			if (pf->IsInFace() && !_pmesh->getFProp(marked, pf))
			{
				face_que.push(pf);
			}
		}
	}
	for (uvFace * pf : uvIterators::MFIterator(_pmesh))
	{
		pf->IsInFace() = _pmesh->getFProp(marked, pf);
	}
	_pmesh->removeFProp(marked);
}

void FaceExtractor::ConstructMeshUV()
{
	//kd-tree
	pcl::KdTreeFLANN<PType> kdtree;
	kdtree.setInputCloud(_pcloud);
	PType point;
	int row = _nr, col = _nc;
	for (uvVertex * pvertex : uvIterators::MVIterator(_pmesh))
	{
		UVPType uv = CalculateOneUV(kdtree, pvertex);
		pvertex->uv() = CPoint2(uv.x / col, 1 - uv.y / row);
	}
	FindBadVertices();
}

UVPType FaceExtractor::CalculateOneUV(pcl::KdTreeFLANN<PType>& kdtree, uvVertex * pvertex)
{
	PType search_point;
	CPoint & point = pvertex->point();
	search_point.x = point[0];
	search_point.y = point[1];
	search_point.z = point[2];
	std::vector<int> pointIdxNKNSearch(NEAREST_NEIGHBOR_NUM);
	std::vector<float> pointNKNSquaredDistance(NEAREST_NEIGHBOR_NUM);
	UVPType uv_data(0, 0, 0);
	if (kdtree.nearestKSearch(search_point, NEAREST_NEIGHBOR_NUM, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)
	{
		//std::cout << "distance square = " << pointNKNSquaredDistance[0] << "\t";
		pvertex->ShortestRadius() = std::sqrt(pointNKNSquaredDistance[0]);
		for (int i = 0; i < NEAREST_NEIGHBOR_NUM; ++i)
		{
			uv_data.x += _pcloud_uv->points[pointIdxNKNSearch[i]].x; // / pointNKNSquaredDistance[i];
			uv_data.y += _pcloud_uv->points[pointIdxNKNSearch[i]].y; // / pointNKNSquaredDistance[i];
		}
		uv_data.x /= NEAREST_NEIGHBOR_NUM; // distance_sum;
		uv_data.y /= NEAREST_NEIGHBOR_NUM; // distance_sum;
	}
	else
	{
		std::cout << "there is no nearest point!" << std::endl;
		getchar();
	}
	return uv_data;
}

void FaceExtractor::FindBadVertices()
{
	std::vector<float> cloud_radius;
	float cloud_r[3], mesh_r[3];
	for (uvVertex * pvertex : uvIterators::MVIterator(_pmesh))
	{
		if (!pvertex->IsMainComponent()) continue;
		cloud_radius.push_back(pvertex->ShortestRadius());
	}
	DoubleAverageValue(cloud_r, cloud_radius);
	for (uvVertex * pvertex : uvIterators::MVIterator(_pmesh))
	{
		if (!pvertex->IsMainComponent()) continue;
		if (pvertex->ShortestRadius() < cloud_r[2])
			pvertex->IsUVBad() = false;
		else
			pvertex->IsUVBad() = true;
	}
}

void FaceExtractor::DoubleAverageValue(float * twovalue, std::vector<float>& dataset)
{
	float average_value = 0;
	for (float value : dataset)
	{
		average_value += value;
	}
	average_value /= dataset.size();
	int count[2];
	twovalue[0] = 0;
	twovalue[1] = 0;
	float values[2] = { 0, 0 };
	do
	{
		values[0] = twovalue[0];
		values[1] = twovalue[1];
		count[0] = 0;
		count[1] = 0;
		twovalue[0] = 0;
		twovalue[1] = 0;
		for (float value : dataset)
		{
			int i = value < average_value ? 0 : 1;
			twovalue[i] += value;
			count[i] ++;
		}
		twovalue[0] /= count[0];
		twovalue[1] /= count[1];
		average_value = (twovalue[0] + twovalue[1]) / 2;
	} while (std::abs(values[0] - twovalue[0]) < 1.0e-8
		&& std::abs(values[1] - twovalue[1]) < 1.0e-8);
	twovalue[2] = average_value;
}

void FaceExtractor::SetDlibLandmarks(std::vector<RowCol> & dlib_landmarks)
{
	_dlib_landmarks.clear();
	_dlib_landmarks.reserve(68);
	for (int index = 0; index < 68; ++index)
	{
		_dlib_landmarks.push_back(dlib_landmarks[index]);
	}
	//std::cout << "Set Dlib Landmark Finished!" << _dlib_landmarks.size() << std::endl;
}

void FaceExtractor::GetRowCol(int & row, int & col, const CPoint2 & uv)
{
	row = (1 - uv[1]) * _nr;
	col = uv[0] * _nc;
}

void FaceExtractor::SetMeshConponent()
{
	//find main vertex
	int row0 = _sub_row0 + _sub_nr / 2,
		col0 = _sub_col0 + _sub_nc / 2;
	float dist0 = 1.0e10;
	uvVertex * pv0 = nullptr;
	uvFace * pf0 = nullptr;
	FPropHandle<bool> mesh_face_handle;
	_pmesh->addFProp(mesh_face_handle, false);
	for (uvVertex * pvertex : uvIterators::MVIterator(_pmesh))
	{
		int row, col;
		GetRowCol(row, col, pvertex->uv());
		float dist = (row - row0) * (row - row0) + (col - col0) * (col - col0);
		if (dist < dist0)
		{
			dist0 = dist;
			pv0 = pvertex;
		}
	}
	pf0 = _pmesh->halfedgeFace(_pmesh->vertexHalfedge(pv0));
	std::queue<uvHalfEdge *> phs;
	pf0->IsInFace() = true;
	_pmesh->getFProp(mesh_face_handle, pf0) = true;
	//assert(pf0->IsInFace());
	for (uvHalfEdge * ph : uvIterators::FHEIterator(pf0))
	{
		if (!_pmesh->isBoundary(ph))
			phs.push(_pmesh->halfedgeSym(ph));
	}
	while (!phs.empty())
	{
		uvHalfEdge * ph = phs.front();
		phs.pop();
		pf0 = _pmesh->halfedgeFace(ph);
		if (pf0->IsMainComponent() 
			&& pf0->IsInFace()
			&& !_pmesh->getFProp(mesh_face_handle, pf0))
		{
			_pmesh->getFProp(mesh_face_handle, pf0) = true;
			ph = _pmesh->halfedgeNext(ph);
			if (!_pmesh->isBoundary(ph))
				phs.push(_pmesh->halfedgeSym(ph));
			ph = _pmesh->halfedgeNext(ph);
			if (!_pmesh->isBoundary(ph))
				phs.push(_pmesh->halfedgeSym(ph));
		}
	}
	for (uvFace * pface : uvIterators::MFIterator(_pmesh))
	{
		pface->IsInFace() = _pmesh->getFProp(mesh_face_handle, pface);
	}
	_pmesh->removeFProp(mesh_face_handle);
}

FaceExtractor::FaceExtractor()
{
	_pmesh = nullptr;
	_sub_row0 = 0;
	_sub_col0 = 0;
	_sub_nr = 0;
	_sub_nc = 0;
}

FaceExtractor::FaceExtractor(
	const char * mesh_path, 
	const char * output_filename, 
	const int & texture_nr, 
	const int & texture_nc, 
	PCType::Ptr pcloud, 
	UVPCType::Ptr pclud_uv)
{
	_pmesh = new uvMesh();
	_pmesh->read_ply(mesh_path);
	_pcloud = pcloud;
	_pcloud_uv = pclud_uv;
	_file_name = output_filename;
	_nr = texture_nr;
	_nc = texture_nc;
	_sub_row0 = 0;
	_sub_col0 = 0;
	_sub_nr = 0;
	_sub_nc = 0;
}

int FaceExtractor::ExtractFace()
{
	if (_pmesh->numVertices() == 0)
	{
		std::cout << "The number of vertices in mesh is 0!" << std::endl;
		return -1;
	}
	_pmesh->SetMainComponent();
	ConstructMeshUV();
	CalculateBoundary();//calculate c r data
	CalculateKeyVertices();
	CutMeshWithPlane();
	SetupFaces();
	MakeFaceMeshConnected();
	if (_pmesh->OutputObj(_file_name) == -1)
		return -1;
	std::cout << "Extract Face Finished!" << std::endl;
	return 0;
}

void FaceExtractor::CalculateBoundary()
{
	//typedef std::pair<int, int> intint;
	std::vector<RowCol> uv_vec;
	int max_r = 0, max_c = 0;
	_sub_row0 = _nr;
	_sub_col0 = _nc;
	for (uvVertex * pvertex : uvIterators::MVIterator(_pmesh))
	{
		if (pvertex->IsUVBad()) continue;
		RowCol uv;
		GetRowCol(uv.row, uv.col, pvertex->uv());
		if (uv.row < _sub_row0) _sub_row0 = uv.row;
		if (uv.row > max_r) max_r = uv.row;
		if (uv.col < _sub_col0) _sub_col0 = uv.col;
		if (uv.col > max_c) max_c = uv.col;
		uv_vec.push_back(uv);
	}
	_sub_nr = max_r - _sub_row0 + 1;
	_sub_nc = max_c - _sub_col0 + 1;
	max_r = _nr;//image row
	max_c = _nc;//image col
	_top_boundary_row = new std::vector<int>(_sub_nc, max_r);
	_bottom_boundary_row = new std::vector<int>(_sub_nc, 0);
	for (RowCol uv : uv_vec)
	{
		if (_top_boundary_row->at(uv.col - _sub_col0) > uv.row)
			_top_boundary_row->at(uv.col - _sub_col0) = uv.row;
		if (_bottom_boundary_row->at(uv.col - _sub_col0) < uv.row)
			_bottom_boundary_row->at(uv.col - _sub_col0) = uv.row;
	}
	for (int i = 1; i < _sub_nc - 1; i++)
	{
		if (_top_boundary_row->at(i) == max_r)
			_top_boundary_row->at(i) = _top_boundary_row->at(i - 1);
		if (_bottom_boundary_row->at(i) == 0)
			_bottom_boundary_row->at(i) = _bottom_boundary_row->at(i - 1);
	}
}

void FaceExtractorAPI::SetUpExtractor(
	const char * mesh_path, 
	const char * output_filename, 
	const int & texture_nr, 
	const int & texture_nc, 
	std::vector<RowCol>& dlib_landmarks, 
	PCType::Ptr pcloud, UVPCType::Ptr pcloud_uv)
{
	_set_up_succeed = true;
	if (texture_nc <= 0 || texture_nr <= 0)
	{
		std::cout << "texture_nc or texture_nr <= 0!" << std::endl;
		_set_up_succeed = false;
	}
	if (pcloud == nullptr || pcloud_uv == nullptr)
	{
		std::cout << "pcloud or pclud is nullptr!" << std::endl;
		_set_up_succeed = false;
	}
	if (pcloud->size() != pcloud_uv->size())
	{
		std::cout << "cloud and uv_cloud are not the same size!" << std::endl;
		_set_up_succeed = false;
	}
	if (dlib_landmarks.size() != 68)
	{
		std::cout << "The size of dlib_landmarks is not 68!" << std::endl;
		_set_up_succeed = false;
	}
	if (!_set_up_succeed)
	{
		std::cout << "Fail to set up!" << std::endl;
		return;
	}
	//main method
	if (_face_extractor != nullptr) delete _face_extractor;
	_face_extractor = new FaceExtractor(mesh_path, output_filename, texture_nr, texture_nc, pcloud, pcloud_uv);
	_face_extractor->SetDlibLandmarks(dlib_landmarks);
}

void FaceExtractorAPI::ExtractFace()
{
	if (!_set_up_succeed) return;
	_face_extractor->ExtractFace();
}
