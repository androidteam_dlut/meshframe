/*
Brief test of Meshlib	2.0
*/
#include <ctime>
#include <list>

#include "Mesh\Vertex.h"
#include "Mesh\Edge.h"
#include "Mesh\Face.h"
#include "Mesh\HalfEdge.h"
#include "Mesh\BaseMesh.h"

#include "Mesh\boundary.h"
#include "Mesh\iterators.h"
#include "Parser\parser.h"

using namespace MeshLib;
using namespace std;

typedef CBaseMesh<CVertex, CEdge, CFace, CHalfEdge>						CMyMesh;

/* global mesh */
CMyMesh mesh;

/*! main function for viewer
*/
int main(int argc, char * argv[])
{
	if (argc != 2)
	{
		std::cout << "Usage: input.m" << std::endl;
		return -1;
	}

	std::string mesh_name(argv[1]);
	if (strutil::endsWith(mesh_name, ".m"))
	{
		mesh.read_m(mesh_name.c_str());
	}

	cout << "#V = " << mesh.numVertices() << endl;
	cout << "#F = " << mesh.numFaces() << endl;
	cout << "#E = " << mesh.numEdges() << endl;
	cout << "#HE = " << mesh.numHalfEdges() << endl;

	typedef  MemoryPool<CVertex>::CMemberT		MemberType;

	/*Test*/
	clock_t  clockBegin, clockEnd;
	clockBegin = clock();

	int n = 0;
	while (n < 10000)
	{
		typename std::vector<MemberType*>::iterator viter_begin = mesh.getVContainer().memoryBlockPtrVec.begin();
		typename std::vector<MemberType*>::iterator viter_end = mesh.getVContainer().memoryBlockPtrVec.end();
		int mySize = mesh.getVContainer().blockSize;
		int a = 0;
		int b = mesh.getVContainer().getCurrentIndex();
		for (typename std::vector<MemberType*>::iterator viter = viter_begin; viter != viter_end; viter++)
		{
			MemberType* v = (*viter);
			for (int i = 0; i < mySize; i++)
			{
				if (v[i].deleted == false)
				{
					v[i].id() = 0;
				}
				a++;
				if (a >= b) break;
			}
			if (a >= b) break;
		}
		n++;
	}
	clockEnd = clock();
	printf("%d����\n", clockEnd - clockBegin);

	n = 0;
	clockBegin = clock();
	while (n < 10000)
	{
		for (auto vertex : mesh.getVContainer())
		{
			CVertex* v = vertex;
			v->id() = 0;
		}
		n++;
	}
	clockEnd = clock();
	printf("%d����\n", clockEnd - clockBegin);

	n = 0;
	clockBegin = clock();
	while (n < 10000)
	{
		for (CMyMesh::MVIterator viter = mesh.getVContainer().begin(); viter != mesh.getVContainer().end(); viter++)
		{
			CVertex* v = (*viter);
			v->id() = 0;
		}
		n++;
	}
	clockEnd = clock();
	printf("%d����\n", clockEnd - clockBegin);

	return 0;
}