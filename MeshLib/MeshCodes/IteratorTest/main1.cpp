/*
Brief test of Meshlib	1.0
*/
#include <ctime>
#include <list>

#include "Mesh\Vertex.h"
#include "Mesh\Edge.h"
#include "Mesh\Face.h"
#include "Mesh\HalfEdge.h"
#include "Mesh\BaseMesh.h"

#include "Mesh\boundary.h"
#include "Mesh\iterators.h"
#include "Parser\parser.h"

using namespace MeshLib;
using namespace std;

typedef CBaseMesh<CVertex, CEdge, CFace, CHalfEdge>						CMyMesh;

/* global mesh */
CMyMesh mesh;

/*! main function for viewer
*/
int main(int argc, char * argv[])
{
	typedef MeshVertexIterator<CVertex, CEdge, CFace, CHalfEdge>			MeshVertexIterator;
	if (argc != 2)
	{
		std::cout << "Usage: input.m" << std::endl;
		return -1;
	}

	std::string mesh_name(argv[1]);
	if (strutil::endsWith(mesh_name, ".m"))
	{
		mesh.read_m(mesh_name.c_str());
	}

	/*Test*/
	clock_t  clockBegin, clockEnd;
	clockBegin = clock();
	int n = 0;
	while (n < 10000)
	{
		for (MeshVertexIterator viter(&mesh); !viter.end(); viter++)
		{
			CVertex* v = (*viter);
			v->id() = 0;
		}

		n++;
	}
	clockEnd = clock();
	printf("%d����\n", clockEnd - clockBegin);

	n = 0;
	clockBegin = clock();
	while (n < 10000)
	{
		for (typename std::list<CVertex*>::iterator iter = mesh.vertices().begin(); iter != mesh.vertices().end(); iter++)
		{
			CVertex* v = (*iter);
			v->id() = 5;
		}
		n++;
	}
	clockEnd = clock();
	printf("%d����\n", clockEnd - clockBegin);

	return 0;
}