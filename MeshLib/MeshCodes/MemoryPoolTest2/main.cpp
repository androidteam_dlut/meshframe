#include <MeshLib2/core/Memory/MemoryPool.h>
#include <MeshLib2/core/Memory/MPIterator.h>
#include <string>
#include <vector>
#include <stdio.h>

#include <ctime>
#include <random>
#define NUM_MEMBERS 2000
#define NUM_TO_D
#define NUM_TO_DELETE 200

std::default_random_engine generator(time(NULL));
std::uniform_int_distribution<int> randInt(0, NUM_TO_DELETE);

class DemoClass {
public:
	DemoClass() :
		str("Demo.")
	{
		i = 100;
	}
	std::string getStr() { return str; };
private:
	int i;
	std::string str;
};

typedef MemoryPool<DemoClass> CClassContainer;

int main(int argc, char ** argv) {
	CClassContainer container(1000, 100);
	std::vector<size_t> idVec;
	std::vector<DemoClass *> ptrVec;

	for (int i = 0; i < NUM_MEMBERS; ++i) {
		size_t id;
		DemoClass * p = container.newMember(id);
		assert(p != NULL);
		idVec.push_back(id);
		ptrVec.push_back(p);
	}

	for (int i = 0; i < NUM_MEMBERS; ++i) {
		DemoClass * p = ptrVec[i];
		printf("Object id: %d\n", idVec[i]);
		printf("Object str: %s\n", p->getStr().c_str());
	}

	for (int i = 0; i < NUM_TO_DELETE; ++i) {
		size_t idToDeleted = randInt(generator);
		printf("Deleted member with id: %d.\n", idToDeleted);

		if (container.deleteMember(idToDeleted)) {
			printf("Delete succeeded!\n");
		}
		else
		{
			printf("Delete failed!\n");
		}

		printf("Container size: %d\n", container.size());
	}

	for (int i = 0; i < NUM_TO_DELETE; ++i)
	{
		size_t id;
		DemoClass * p = container.newMember(id);
		assert(p != NULL);
		printf("Container capacity: %d\n", container.capacity());
		printf("Container size: %d\n", container.size());
	}

	int numMembers = 0;
	for (DemoClass * pClass : container) {
		printf("Object str: %s\n", pClass->getStr().c_str());
		++numMembers;
	}

	printf("numMembers: %d", numMembers);
	printf("Container Size: %d", container.size());
}
