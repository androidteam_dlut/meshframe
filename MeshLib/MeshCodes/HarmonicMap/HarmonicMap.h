#pragma once
#include <MeshLib/core/Mesh/MeshHeaders.h>
#include <MeshLib/toolbox/IteratorsPacking.h>
#include <iostream>
#include <cmath>
#include <omp.h>

#define STRING_EN(pE) (pE->k*pow((pMesh->edgeVertex1(pE)->point()-pMesh->edgeVertex2(pE)->point()).norm(),2))
#define HSTR_EN(phe) (phe == NULL ? 0 : -((pMesh->halfedgeNext(phe))*(IF::halfedgePrev(phe)->vec())) \
/ ((IF::halfedgeNext(phe)->vec())^(IF::halfedgePrev(phe)->vec())).norm())
#define PRINTING_COUNT 1000
#define ARC_TO_COORD(arc) sin(arc),cos(arc), 0 

namespace MeshLib {
	struct _edgeHarmonic {
		double k;
	};
	struct _vertexHarmonic {
		CPoint newPos;
	};

	struct _halfEdgeHarmonic
	{
		float oLength;
	};

	template<typename V, typename E,  typename F, typename HE>
	class HarmonicMapCore {
	private:
		typedef CBaseMesh<V, E, F, HE> MeshType;

		typedef MeshVertexIterator<V, E, F, HE>			MeshVertexIterator;
		typedef MeshEdgeIterator<V, E, F, HE>			MeshEdgeIterator;
		typedef MeshFaceIterator<V, E, F, HE>			MeshFaceIterator;
		typedef MeshHalfEdgeIterator<V, E, F, HE>		MeshHalfEdgeIterator;

		typedef VertexVertexIterator<V, E, F, HE>		VertexVertexIterator;
		typedef VertexEdgeIterator<V, E, F, HE>			VertexEdgeIterator;
		typedef VertexFaceIterator<V, E, F, HE>			VertexFaceIterator;
		typedef VertexInHalfedgeIterator<V, E, F, HE>	VertexInHalfedgeIterator;
		typedef VertexOutHalfedgeIterator<V, E, F, HE>	VertexOutHalfedgeIterator;

		typedef FaceVertexIterator<V, E, F, HE>			FaceVertexIterator;
		typedef FaceEdgeIterator<V, E, F, HE>			FaceEdgeIterator;
		typedef FaceHalfedgeIterator<V, E, F, HE>		FaceHalfedgeIterator;
	public:
		HarmonicMapCore() {};
		void setInputMesh(MeshType* newpMesh);
		void setInitalMap();
		void map();
		void centerVisualMap(CPoint center);
		void setStep(double newStep);
		void setStopEpsion(double newEpsion);
		void setStopGradientThreshold(double newGradientThreshold);
		double totalEnergy();
		bool adjustPointVisualOneStep();
		void iterativelyAdjustPoint();
		void iterativelyAdjustPoint_parallel();
		bool dynamicStep = false;
		void setDynamicStepSize(double newSize);

		enum ConvergeCondition {minError, maxGradient};
		ConvergeCondition convergeCondition = maxGradient;


	private:
		MeshType * pMesh;
		void calculateStringConstraints();
		void updateStep();
		double halfedgeStringEnergy(HE * pHE);
		double step = 0.001;
		double Epsion = 0.00001;
		double gradientThreshold = 0.00001;
		double dynamicStepSize = 50;
		bool hasConverged();

		int numV = 0;
		double minErr = 1000000000.0f;
		double maxGradientNorm = 0;

		CPoint halfEdgeVec(HE * pHE);

	};

	template <typename M>
	struct HarmonicMap : public HarmonicMapCore<typename M::VType, typename M::EType, typename M::FType, typename M::HEType> {};

	template<typename V, typename E, typename F, typename HE>
	inline void HarmonicMapCore<V, E, F, HE>::setInputMesh(MeshType* newpMesh)
	{
		pMesh = newpMesh;
		numV = pMesh->vertices().size();
		calculateStringConstraints();
	}

	template<typename V, typename E, typename F, typename HE>
	inline void HarmonicMapCore<V, E, F, HE>::setInitalMap()
	{
		CBoundary<V, E, F, HE> boundary(pMesh);
		auto loop = boundary.loops().front(); 
		
		double l = loop->length(), lt = 0;
		for (auto pHE : loop->halfedges()) { 
			pHE->oLength = pMesh->edgeLength(pMesh->halfedgeEdge(pHE)); 
		}
		for (auto pV : pMesh->vertices()) {
			pV->point() = CPoint(0, 0, 0);
		};
		for (auto pHE : loop->halfedges()) {
			auto pV = pMesh->halfedgeTarget(pHE);
			pV->point() = CPoint(ARC_TO_COORD(2 * 3.1415926*(lt / l))); 
			lt += pHE->oLength;
		} 
	}

	template<typename V, typename E, typename F, typename HE>
	inline void HarmonicMapCore<V, E, F, HE>::map()
	{
		std::cout << "Iteratively Adjusting Point.\n";
		iterativelyAdjustPoint();
	}
	template<typename V, typename E, typename F, typename HE>
	inline void HarmonicMapCore<V, E, F, HE>::calculateStringConstraints()
	{
		for (MeshEdgeIterator meIter(pMesh); !meIter.end(); ++meIter) {
			E* pE = *meIter;

			pE->k = halfedgeStringEnergy(pMesh->edgeHalfedge(pE, 0)) + halfedgeStringEnergy(pMesh->edgeHalfedge(pE, 1));
			//std::cout << "Edge Weight: " << pE->k << "\n";
		}
	}

	template<typename V, typename E, typename F, typename HE>
	inline void HarmonicMapCore<V, E, F, HE>::setDynamicStepSize(double newSize)
	{
		dynamicStepSize = newSize;
	}

	template<typename V, typename E, typename F, typename HE>
	inline bool HarmonicMapCore<V, E, F, HE>::adjustPointVisualOneStep()
	{
		int numV = pMesh->vertices().size();
        static int count = 0;
		double formalEnergy, currentEnergy;
		currentEnergy = totalEnergy();
		CPoint (0, 0, 0);
		for (auto pV : pMesh->vertices()) {
			if (pV->boundary()) {
				continue;
			}
			CPoint nP;
			CPoint& P = pV->point();
			double totalK = 0.0;
	
			//for (VVIter(pV, pNV)) {
			for (VertexVertexIterator vIter(pV); !vIter.end(); ++vIter){
				V * pNV = *vIter;
				auto pE = pMesh->vertexEdge(pV, pNV);
				nP += pNV->point()*pE->k;
				totalK += pE->k;
			}
			pV->point() = nP / totalK;
		}
		//for (MeshVertexIterator mvIter(pMesh); !mvIter.end(); ++mvIter) {
		//	V * pV = *mvIter;
		//	if (pV->boundary()) {
		//		continue;
		//	}
		//	CPoint& P = pV->point();
		//	P = pV->newPos;
		//}
		formalEnergy = currentEnergy;
		currentEnergy = totalEnergy();
		
		//std::cout << "New Harmonic Energy: " << currentEnergy << std::endl;
		//if (abs(formalEnergy - currentEnergy) > Epsion ) {
        ++count;
        if (count < 1000 ) {
            return false;
		}
		else {
			return true;
		}
	}

	template<typename V, typename E, typename F, typename HE>
	inline double HarmonicMapCore<V, E, F, HE>::halfedgeStringEnergy(HE * pHE)
	{
		if (pHE == NULL) {
			return 0;
		}
		CPoint v0 = halfEdgeVec(pMesh->halfedgeNext(pHE));
		CPoint v1 = halfEdgeVec(pMesh->halfedgePrev(pHE));

		return -(v0*v1)/(v0^v1).norm();
	}
	template<typename V, typename E, typename F, typename HE>
	inline bool HarmonicMapCore<V, E, F, HE>::hasConverged()
	{
		switch (convergeCondition)
		{
		case minError:
			if (minErr < Epsion) {
				return true;
			}
			else {
				return false;
			}
			break;
		case maxGradient:
			if (maxGradientNorm < gradientThreshold) {
				return true;
			}
			else
			{
				return false;
			}
		default:
			break;
		}
		return true;
	}
	template<typename V, typename E, typename F, typename HE>
	inline CPoint HarmonicMapCore<V, E, F, HE>::halfEdgeVec(HE * pHE)
	{
		return pHE->target()->point() - pHE->source()->point();
	}
	template<typename V, typename E, typename F, typename HE>
	inline double HarmonicMapCore<V, E, F, HE>::totalEnergy()
	{
		double e = 0.0; 
		for (MeshEdgeIterator meIter(pMesh); !meIter.end(); ++meIter) {
			E * pE = *meIter;
			e += STRING_EN(pE); 
		} 
		return e;
	}
}