#include <MeshLib/core/Mesh/MeshHeaders.h>
#include "HarmonicMap.h"
using namespace MeshLib;
class CHEHm : public CHalfEdge, public _halfEdgeHarmonic {};
class CEdgeHm : public CEdge, public _edgeHarmonic {};
class CVertexHm : public CVertex, public _vertexHarmonic {};
typedef CBaseMesh<CVertexHm, CEdgeHm, CFace, CHEHm> M;
typedef FaceVertexIterator<CVertexHm, CEdgeHm, CFace, CHEHm>			FVIterator;

int main(int argc, char ** argv) {
	M * pM = new M;
	M * pM1 = new M;

	pM->read_obj(argv[1]);
	std::vector<float> pBuf;
	std::vector<short> fBuf;

	for (CVertexHm * pV : pM->vertices()) {
		pBuf.push_back(pV->point()[0]);
		pBuf.push_back(pV->point()[1]);
		pBuf.push_back(pV->point()[2]);
	}

	for (CFace * pF : pM->faces()) {
		for (FVIterator fvIter(pF); !fvIter.end(); ++fvIter) {
			CVertexHm * pV = *fvIter;
			fBuf.push_back(pV->id()-1);
		}
	}
	pM1->read_buffer(pBuf.data(), pM->vertices().size(), fBuf.data(), pM->faces().size());

	HarmonicMap<M> hMap;
	hMap.setInputMesh(pM1);
	hMap.setInitalMap();

	bool succeeded = false;
	while (!succeeded) {
		succeeded = hMap.adjustPointVisualOneStep();
	}
	pM1->write_obj("outputs.obj");
}
