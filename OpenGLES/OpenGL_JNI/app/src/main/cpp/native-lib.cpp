#include <jni.h>
#include <string>

struct MeshData{
    jshort * pFacesBuf;
    jfloat * pPointsBuf;
    jint numV;
    jint numF;

};

extern "C"
JNIEXPORT jstring

JNICALL
Java_anka_opengl_1jni_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT jlong JNICALL
Java_anka_opengl_1jni_MeshProcesser_n_1initialize(JNIEnv *env, jobject instance) {

    MeshData * pMeshData = new MeshData;
    return  (jlong)pMeshData;

}

extern "C"
JNIEXPORT jboolean JNICALL
Java_anka_opengl_1jni_MeshProcesser_n_1loadMesh(JNIEnv *env, jobject instance, jlong p32MeshDataAdr,
                                                jobject points, jobject faces)
{
    MeshData *pMeshData = (MeshData *) p32MeshDataAdr;
    pMeshData->pPointsBuf = (jfloat *) env->GetDirectBufferAddress(points);
    pMeshData->numV = (jint)env->GetDirectBufferCapacity(points) / 3;
    pMeshData->pFacesBuf = (jshort *) env->GetDirectBufferAddress(faces);
    pMeshData->numF = (jint)env->GetDirectBufferCapacity(faces) / 3;

    return (jboolean) true;
}
extern "C"
JNIEXPORT jlong JNICALL
Java_anka_opengl_1jni_MeshProcesser_n_1numV(JNIEnv *env, jobject instance, jlong p32MeshDataAdr) {

    MeshData * pMeshData = (MeshData *) p32MeshDataAdr;

    return  pMeshData->numV;
}

extern "C"
JNIEXPORT jlong JNICALL
Java_anka_opengl_1jni_MeshProcesser_n_1numF(JNIEnv *env, jobject instance, jlong p32MeshDataAdr) {

    MeshData * pMeshData = (MeshData *) p32MeshDataAdr;

    return  pMeshData->numF;

}extern "C"
JNIEXPORT void JNICALL
Java_anka_opengl_1jni_MeshProcesser_n_1shrinkBy(JNIEnv *env, jobject instance, jlong p32MeshDataAdr,
                                                jfloat level) {
    MeshData * pMeshData = (MeshData *) p32MeshDataAdr;
    for (int i = 0; i < 3 * pMeshData->numV; ++i){
        pMeshData->pPointsBuf[i] *= level;
    }
}