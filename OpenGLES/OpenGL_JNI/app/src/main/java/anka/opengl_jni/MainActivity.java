package anka.opengl_jni;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.content.Context;
import android.opengl.GLSurfaceView;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    private GLSurfaceView mGLView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create a GLSurfaceView instance and set it
        // as the ContentView for this Activity.
        mGLView = new MyGLSurfaceView(this);
        setContentView(mGLView);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
}


class MyGLSurfaceView extends GLSurfaceView {

    private final MyGLRenderer mRenderer;

    public MyGLSurfaceView(Context context){
        super(context);

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);

        mRenderer = new MyGLRenderer();

        InputStream inputStream = context.getResources().openRawResource(R.raw.face);
        ObjFile objFile = new ObjFile();
        objFile.readFile(inputStream);
        objFile.normalize();
        mRenderer.setObjFile(objFile);

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer);

    }

}
