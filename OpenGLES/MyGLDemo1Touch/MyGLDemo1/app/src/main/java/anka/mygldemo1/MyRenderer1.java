package anka.mygldemo1;


//import android.opengl.EGLConfig;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

public class MyRenderer1 implements GLSurfaceView.Renderer {

    private static final String VERTEX_SHADER =
            "attribute vec4 position;\n"+
            "uniform mat4 matrix;\n"+
            "void main() {\n"+
            "    gl_Position = matrix * position;\n"+
            "}";

    private static final String FRAGMENT_SHADER =
            "precision mediump float;\n"+
            "void main() {\n"+
            "    gl_FragColor = vec4(1, 0.5, 0, 1.0);\n"+
            "}";

    private int mProgram;
    private int mPositionHandle;
    private int mNormalHandle;

    private ObjFile objFile;

    MyRenderer1() {

    }

    static int loadShader(int type, String shaderCode) {
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        mProgram = GLES20.glCreateProgram();
        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, VERTEX_SHADER);
        int fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, FRAGMENT_SHADER);
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);
        GLES20.glLinkProgram(mProgram);

        GLES20.glUseProgram(mProgram);

    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) {
        float[] projectionMatrix = new float[16];
        float[] viewMatrix = new float[16];

        float[] productMatrix = new float[16];

        Matrix.frustumM(projectionMatrix, 0,
                -1, 1,
                -1, 1,
                2, 9);

        Matrix.setLookAtM(viewMatrix, 0,
                0, 3, -4,
                0, 0, 0,
                0, 1, 0);

        Matrix.multiplyMM(productMatrix, 0,
                projectionMatrix, 0,
                viewMatrix, 0);

        int MVPmatrix = GLES20.glGetUniformLocation(mProgram, "matrix");
        GLES20.glUniformMatrix4fv(MVPmatrix, 1, false, productMatrix, 0);

        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "position");
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glVertexAttribPointer(mPositionHandle,
                3, GLES20.GL_FLOAT, false, 3 * 4, objFile.verticesBuffer);;

        GLES20.glDrawElements(GLES20.GL_TRIANGLES,
                objFile.facesList.size() * 3, GLES20.GL_UNSIGNED_SHORT, objFile.facesBuffer);

        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }

    public void setObjFile(ObjFile objF){
        objFile = objF;
    }
}