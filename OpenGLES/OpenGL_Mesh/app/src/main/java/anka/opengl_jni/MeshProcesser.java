package anka.opengl_jni;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

/**
 * Created by lenovo001 on 2017/11/26.
 */

public class MeshProcesser {
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    public void initialize(){
        p32MeshDataAdr = n_initialize();
    }

    public boolean loadMesh(FloatBuffer points, ShortBuffer faces){
        return n_loadMesh(p32MeshDataAdr, points, faces);
    }

    public int numV(){
        return n_numV(p32MeshDataAdr);
    }

    public int numF(){
        return n_numF(p32MeshDataAdr);
    }

    public  void  shrinkBy(float level){
        n_shrinkBy(p32MeshDataAdr, level);
    }

    private native long n_initialize();

    private native boolean n_loadMesh(long p32MeshDataAdr, FloatBuffer points, ShortBuffer faces);

    private native int n_numV(long p32MeshDataAdr);

    private native int n_numF(long p32MeshDataAdr);

    private native void n_shrinkBy(long p32MeshDataAdr, float level);



    private long p32MeshDataAdr;

}
