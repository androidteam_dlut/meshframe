package anka.opengl_jni;

import android.graphics.PointF;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.widget.Toast;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {
    /*View*/
    private MyGLSurfaceView mGLView;
    /*Number of touching point*/
    private int NumTouchPoint = 0;
    /*Touch Mode: NONE, ROTATE, ZOOM_DRAG*/
    private static final int NONE = 0;
    private static final int ROTATE = 1;
    private static final int ZOOM_DRAG = 2;
    private int mode = NONE;
    /*View Height*/
    private float ViewHeight;
    /*View Width*/
    private float ViewWidth;
    /*First finger's previous touching point*/
    private PointF PrePoint1 = new PointF();
    /*Second finger's previous touching point*/
    private PointF PrePoint2 = new PointF();
    /*First finger's moving vector*/
    private PointF MoveVector1 = new PointF();
    /*Second finger's moving vector*/
    private PointF MoveVector2 = new PointF();
    /*Previous scale*/
    private float PreScale;
    /*New scale*/
    private float NewScale;
    /*Max scaling span*/
    private static final float MAX_SCALE = 1.1f;
    /*Minimum scaling span*/
    private static final float MIN_SCALE = 0.98f;
    /*Long press wait time*/
    private static final int LONG_PRESS_TIME = 1000;
    /*Handler, used to open threads*/
    private Handler mBaseHandler = new Handler();
    /*Flag to mark long press if will be called*/
    private boolean IfInLongPress = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Get the Height and Width of machine's window*/
        WindowManager wm = this.getWindowManager();
        ViewHeight = wm.getDefaultDisplay().getHeight();
        ViewWidth = wm.getDefaultDisplay().getWidth();
        /*Create a GLSurfaceView instance and set it
        *as the ContentView for this Activity.*/
        mGLView = new MyGLSurfaceView(this, ViewHeight, ViewWidth);
        setContentView(mGLView);
        /*Create listener to monitor touch event*/
        mGLView.setOnTouchListener(this);
        /*Create new view*/
        LinearLayout MyLinearLayout = new LinearLayout(this);
        /*Create button*/
        Button Btn_function = new Button(this);
        Btn_function.setText("Function");
        /*Add button into view*/
        MyLinearLayout.addView(Btn_function);
        addContentView(MyLinearLayout,
                new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        /*Create button listener*/
        Btn_function.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,
                        "Invoking Harmonic Function!",Toast.LENGTH_SHORT).show();
                mGLView.mRenderer.runHarmonic();
            }
        });
    }

    @Override
    /*Override onTouch()*/
    public boolean onTouch(View v, MotionEvent event)
    {
        /*Get the number of touch points*/
        NumTouchPoint = event.getPointerCount();
        /*Invoking different function according to different gestures*/
        switch(event.getAction() & MotionEvent.ACTION_MASK){
            /*First finger touch*/
            case MotionEvent.ACTION_DOWN:
                /*If the number of touch point is one, begin threads of long press*/
                if(NumTouchPoint == 1)
                {
                    IfInLongPress = true;
                    mBaseHandler.postDelayed(LongPressTask,LONG_PRESS_TIME);
                }
                /*Record the touch point*/
                PrePoint1.set(event.getX(), event.getY());
                /*Using MyGLRenderer.rotateObject() to rotate model*/
                mGLView.mRenderer.rotateObject(event.getX(), event.getY(), true);
                /*Reset Touch Mode*/
                mode = ROTATE;
                break;
            /*Second finger touch*/
            case MotionEvent.ACTION_POINTER_DOWN:
                if(IfInLongPress == true)
                {
                    /*Stop LongPress Runnable*/
                    mBaseHandler.removeCallbacks(LongPressTask);
                    IfInLongPress = false;
                }
                /*Record the touch point*/
                PrePoint2.set(event.getX(), event.getY());
                /*Record the previous span between first finger and second finger*/
                PreScale = CalculateDistance(event.getX(0), event.getY(0)
                        ,event.getX(1),event.getY(1));
                /*Record the current span between first finger and second finger*/
                NewScale = PreScale;
                /*Reset Touch Mode*/
                mode = ZOOM_DRAG;
                break;
            /*Finger touch move*/
            case MotionEvent.ACTION_MOVE:
                float distance =
                        CalculateDistance(event.getX(), event.getY(), PrePoint1.x, PrePoint1.y);
                if(IfInLongPress == true && distance > 1.0f)
                {
                    /*Stop LongPress Runnable*/
                    mBaseHandler.removeCallbacks(LongPressTask);
                    IfInLongPress = false;
                }
                /*ROTATE MODE*/
                if(mode == ROTATE)
                {
                    if(distance > 1.0f)
                    {
                        Toast.makeText(MainActivity.this,
                                "ROTATE",Toast.LENGTH_SHORT).show();
                    }
                    /*Using MyGLRenderer.rotateObject() to rotate model*/
                    mGLView.mRenderer.rotateObject(event.getX(), event.getY(), false);
                }
                /*ZOOM_DRAG MODE*/
                else if (mode == ZOOM_DRAG)
                {
                    /*Get the moving vector of finger touch*/
                    MoveVector1.x = (event.getX(0) - PrePoint1.x) / ViewWidth;
                    MoveVector1.y = (event.getY(0) - PrePoint1.y) / ViewHeight;
                    MoveVector2.x = (event.getX(1) - PrePoint2.x) / ViewWidth;
                    MoveVector2.y = (event.getY(1) - PrePoint2.y) / ViewHeight;
                    /*Judge if the direction of moving is the same*/
                    if(MoveVector1.x * MoveVector2.x > 0 && MoveVector1.y * MoveVector2.y > 0)
                    {
                        Toast.makeText(MainActivity.this,
                                "MOVE",Toast.LENGTH_SHORT).show();
                        /*Using MyGLRenderer.moveEye to move eye's position*/
                        mGLView.mRenderer.moveEye(MoveVector1.x,MoveVector1.y);
                    }
                    else
                    {
                        Toast.makeText(MainActivity.this,
                                "SCALE",Toast.LENGTH_SHORT).show();
                       /*Record the current span between first finger and second finger*/
                        NewScale = CalculateDistance(event.getX(0),
                                event.getY(0),
                                event.getX(1),
                                event.getY(1));
                        /*Calculate current scale*/
                        float CurrentScale = (PreScale / NewScale);
                        /*Scale limit*/
                        if(CurrentScale > MAX_SCALE)
                        {
                            CurrentScale = MAX_SCALE;
                        }
                        if(CurrentScale < MIN_SCALE)
                        {
                            CurrentScale = MIN_SCALE;
                        }
                        /*Using MyGLRenderer.setDistanceEyeZ(float scale) to scale model*/
                        mGLView.mRenderer.setDistanceEyeZ(CurrentScale);
                        /*Reset PreScale*/
                        PreScale = NewScale;
                    }
                    /*Reset PrePoint*/
                    PrePoint1.x = event.getX(0);
                    PrePoint1.y = event.getY(0);
                    PrePoint2.x = event.getX(1);
                    PrePoint2.y = event.getY(1);
                }
                break;
            /*Finger action up*/
            case MotionEvent.ACTION_UP:
                if(IfInLongPress == true)
                {
                    /*Stop LongPress Runnable*/
                    mBaseHandler.removeCallbacks(LongPressTask);
                    IfInLongPress = false;
                }
                mode = NONE;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                break;
            default:
        }
        return true;
    }
    /*Calculating distance between pointA and pointB*/
    private float CalculateDistance(float x1, float y1, float x2, float y2)
    {
        return (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }
    /*Thread to control long press task*/
    private Runnable LongPressTask = new Runnable()
    {
        @Override
        public void run()
        {
            /*Using MyGLRenderer.resetView() to reset user's view*/
            Toast.makeText(MainActivity.this,"RESET",Toast.LENGTH_SHORT).show();
            mGLView.mRenderer.resetView();
        }
    };
}


class MyGLSurfaceView extends GLSurfaceView {

    public final MyGLRenderer mRenderer;

    /*View Height*/
    private float ViewHeight;
    /*View Width*/
    private float ViewWidth;

    public MyGLSurfaceView(Context context, float height, float width){
        super(context);

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);


        ViewHeight = height;
        ViewWidth = width;

        mRenderer = new MyGLRenderer(ViewHeight, ViewWidth);

        InputStream inputStream = context.getResources().openRawResource(R.raw.face);
        ObjFile objFile = new ObjFile();
        objFile.readFile(inputStream);
        objFile.normalize();
        mRenderer.setObjFile(objFile);

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer);

    }

}
