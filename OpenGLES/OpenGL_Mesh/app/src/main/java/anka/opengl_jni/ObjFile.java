package anka.opengl_jni;
import android.content.Context;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import anka.opengl_jni.R;

/**
 * Created by lenovo001 on 2017/11/20.
 */

public class ObjFile {
    public List<String> verticesList;
    public List<String> facesList;
    public List<String> normalList;
    //private Context context;

    public FloatBuffer verticesBuffer;
    public FloatBuffer normalsBuffer;
    public ShortBuffer facesBuffer;

    public ObjFile() {
        verticesList = new ArrayList<>();
        normalList = new ArrayList<>();;
        facesList = new ArrayList<>();
        //context = new_context;
    }

    public boolean readFile(InputStream iStream){
        // Open the OBJ file with a Scanner
        //InputStream inputStream = context.getResources().openRawResource(R.raw.torus);
        Scanner scanner = new Scanner(iStream);
        // Loop through all its lines
        while(scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if(line.startsWith("v ")) {
                // Add vertex line to list of vertices
                verticesList.add(line);
            } else if(line.startsWith("f ")) {
                // Add face line to faces list
                facesList.add(line);
            } else if (line.startsWith("vn ")){
                normalList.add(line);
            }
        }

        // Close the scanner
        scanner.close();

        // Create buffer for vertices
        ByteBuffer buffer1 = ByteBuffer.allocateDirect(verticesList.size() * 3 * 4);
        buffer1.order(ByteOrder.nativeOrder());
        verticesBuffer = buffer1.asFloatBuffer();

        ByteBuffer buffer2 = ByteBuffer.allocateDirect(facesList.size() * 3 * 2);
        buffer2.order(ByteOrder.nativeOrder());
        facesBuffer = buffer2.asShortBuffer();

        ByteBuffer buffer3 = ByteBuffer.allocateDirect(normalList.size() * 3 * 4);
        buffer3.order(ByteOrder.nativeOrder());
        normalsBuffer = buffer3.asFloatBuffer();

        for(String vertex: verticesList) {
            String coords[] = vertex.split(" "); // Split by space
            float x = Float.parseFloat(coords[1]);
            float y = Float.parseFloat(coords[2]);
            float z = Float.parseFloat(coords[3]);
            verticesBuffer.put(x);
            verticesBuffer.put(y);
            verticesBuffer.put(z);
        }
        verticesBuffer.position(0);

        for(String normal: normalList) {
            String coords[] = normal.split(" "); // Split by space
            float nx = Float.parseFloat(coords[1]);
            float ny = Float.parseFloat(coords[2]);
            float nz = Float.parseFloat(coords[3]);
            normalsBuffer.put(nx);
            normalsBuffer.put(ny);
            normalsBuffer.put(nz);
        }
        normalsBuffer.position(0);

        for(String face: facesList) {
            String vertexIndices[] = face.split(" ");
            short vertex1 = Short.parseShort(vertexIndices[1]);
            short vertex2 = Short.parseShort(vertexIndices[2]);
            short vertex3 = Short.parseShort(vertexIndices[3]);
            facesBuffer.put((short)(vertex1 - 1));
            facesBuffer.put((short)(vertex2 - 1));
            facesBuffer.put((short)(vertex3 - 1));
        }
        facesBuffer.position(0);

        return true;
    }

    public void normalize(){
        float x, y, z, cx = 0f, cy=0f, cz=0f, xScale=0f, yScale=0f, zScale=0f, scale;
        for (int i = 0 ; i < verticesBuffer.capacity(); i+=3){
            cx += verticesBuffer.get(i);
            cy += verticesBuffer.get(i+1);
            cz += verticesBuffer.get(i+2);
        }

        cx/=verticesList.size();
        cy/=verticesList.size();
        cz/=verticesList.size();

        for (int i = 0 ; i < verticesBuffer.capacity(); i+=3){
            x = verticesBuffer.get(i);
            y = verticesBuffer.get(i+1);
            z = verticesBuffer.get(i+2);

            x -= cx;
            y -= cy;
            z -= cz;

            if (x > xScale){
                xScale = x;
            }
            if (y > yScale){
                yScale = y;
            }
            if (z > zScale){
                zScale = z;
            }

            verticesBuffer.put(i, x);
            verticesBuffer.put(i+1, y);
            verticesBuffer.put(i+2, z);
        }
        scale = Math.max(xScale, Math.max(yScale, zScale));
        for (int i = 0; i < verticesBuffer.capacity(); ++i ){
            x = verticesBuffer.get(i);
            verticesBuffer.put(i, x/scale);
        }
    }

}