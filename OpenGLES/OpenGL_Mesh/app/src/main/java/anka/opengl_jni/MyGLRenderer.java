package anka.opengl_jni;

//import android.opengl.EGLConfig;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

public class MyGLRenderer implements GLSurfaceView.Renderer {

    private static final String VERTEX_SHADER =
            "uniform mat4 u_MVPMatrix;      \n"     // A constant representing the combined model/view/projection matrix.
                    + "uniform mat4 u_MVMatrix;       \n"     // A constant representing the combined model/view matrix.
                    //+ "uniform vec3 u_LightPos;       \n"     // The position of the light in eye space.

                    + "attribute vec4 a_Position;     \n"     // Per-vertex position information we will pass in.
                    //+ "attribute vec4 a_Color;        \n"     // Per-vertex color information we will pass in.
                    + "attribute vec3 a_Normal;       \n"     // Per-vertex normal information we will pass in.

                    + "varying vec4 v_Color;          \n"     // This will be passed into the fragment shader.

                    + "void main()                    \n"     // The entry point for our vertex shader.
                    + "{                              \n"
                    + "   vec3 u_LightPos = vec3(0.0, 0.0, -10.0);                                  \n"
                    + "   vec4 a_Color = vec4(0.5, 0.5, 0.5, 1.0);                                \n"
                    // Transform the vertex into eye space.
                    + "   vec3 modelViewVertex = vec3(u_MVMatrix * a_Position);              \n"
                    // Transform the normal's orientation into eye space.
                    + "   vec3 modelViewNormal = vec3(u_MVMatrix * vec4(a_Normal, 0.0));     \n"
                    // Will be used for attenuation.
                    + "   float distance = length(u_LightPos - modelViewVertex);             \n"
                    // Get a lighting direction vector from the light to the vertex.
                    + "   vec3 lightVector = normalize(u_LightPos - modelViewVertex);        \n"
                    // Calculate the dot product of the light vector and vertex normal. If the normal and light vector are
                    // pointing in the same direction then it will get max illumination.
                    + "   float diffuse = max(dot(modelViewNormal, lightVector), 0.1);       \n"
                    // Attenuate the light based on distance.
                    + "   diffuse = diffuse * (1.0 / (1.0 + (0.25 * distance * distance)));  \n"
                    // Multiply the color by the illumination level. It will be interpolated across the triangle.
                    + "   v_Color = a_Color * diffuse + vec4(0.2, 0.2, 0.2, 0);                                       \n"
                    // gl_Position is a special variable used to store the final position.
                    // Multiply the vertex by the matrix to get the final point in normalized screen coordinates.
                    + "   gl_Position = u_MVPMatrix * a_Position;                            \n"
                    + "}";

    private static final String FRAGMENT_SHADER =
            "precision mediump float;       \n"     // Set the default precision to medium. We don't need as high of a
                    // precision in the fragment shader.
                    + "varying vec4 v_Color;          \n"     // This is the color from the vertex shader interpolated across the
                    // triangle per fragment.
                    + "void main()                    \n"     // The entry point for our fragment shader.
                    + "{                              \n"
                    + "   gl_FragColor = v_Color;     \n"     // Pass the color directly through the pipeline.
                    + "}                              \n";


    private int mProgram;
    private int mPositionHandle;
    private int mNormalHandle;

    private int srinkLevel = 1;
    private ObjFile objFile;

    private float[] mProjectionMatrix = new float[16];
    private float[] viewMatrix = new float[16];

    private float[] productMatrix = new float[16];

    private MeshProcesser mMeshProcesser;

    private float distanceEyeZ = -4;

    /*Rotation angle*/
    private float rotationAngleX = 0.0f;
    private float rotationAngleY = 0.0f;

    private float xdiff = 0.0f;
    private float ydiff = 0.0f;

    /*Eye move distance*/
    private float moveX = 0.0f;
    private float moveY = 0.0f;

    /*View ratio*/
    private float ratio;

    /*If Harmonic*/
    private boolean ifHarmonic = false;

    MyGLRenderer(float height, float width) {
       /*Get the screen height and width*/
        ratio =  width / height;
    }

    static int loadShader(int type, String shaderCode) {
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        mProgram = GLES20.glCreateProgram();
        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, VERTEX_SHADER);
        int fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, FRAGMENT_SHADER);
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);

        GLES20.glLinkProgram(mProgram);
        GLES20.glUseProgram(mProgram);

    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        ratio = (float) width / height;
        /*this projection matrix is applied to object coordinates*/
        /*in the onDrawFrame() method*/
        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 2, 10);
    }

    @Override
    public void onDrawFrame(GL10 unused) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear( GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT );

        /*this projection matrix is applied to object coordinates
        * in the onDrawFrame() method*/
        Matrix.frustumM(mProjectionMatrix, 0,
                -ratio, ratio,
                -1, 1,
                2, 10);

        Matrix.setLookAtM(viewMatrix, 0,
                moveX, moveY, distanceEyeZ,
                moveX, moveY, 0,
                0, 1, 0);

        Matrix.multiplyMM(productMatrix, 0,
                mProjectionMatrix, 0,
                viewMatrix, 0);

        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glCullFace(GLES20.GL_FRONT);

         /*Rotate model*/
        Matrix.rotateM( productMatrix, 0, rotationAngleX,1,0,0);
        Matrix.rotateM( productMatrix, 0, rotationAngleY,0,-1,0);

        int MVmatrix = GLES20.glGetUniformLocation(mProgram, "u_MVMatrix");
        GLES20.glUniformMatrix4fv(MVmatrix, 1, false, viewMatrix, 0);

        int MVPmatrix = GLES20.glGetUniformLocation(mProgram, "u_MVPMatrix");
        GLES20.glUniformMatrix4fv(MVPmatrix, 1, false, productMatrix, 0);

        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "a_Position");
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glVertexAttribPointer(mPositionHandle,
                3, GLES20.GL_FLOAT, false, 3 * 4, objFile.verticesBuffer);

        if(ifHarmonic)
        {
            mMeshProcesser.shrinkBy(0.999f);
        }

        mNormalHandle = GLES20.glGetAttribLocation(mProgram, "a_Normal");
        GLES20.glEnableVertexAttribArray(mNormalHandle);
        GLES20.glVertexAttribPointer(mNormalHandle,
                3, GLES20.GL_FLOAT, false, 3 * 4, objFile.normalsBuffer);


        GLES20.glDrawElements(GLES20.GL_TRIANGLES,
                objFile.facesList.size() * 3, GLES20.GL_UNSIGNED_SHORT, objFile.facesBuffer);

        GLES20.glDisableVertexAttribArray(mPositionHandle);
        GLES20.glDisableVertexAttribArray(mNormalHandle);
    }
    /*Set object*/
    public void setObjFile(ObjFile objF){
        ifHarmonic = false;
        objFile = objF;
        mMeshProcesser = new MeshProcesser();
        mMeshProcesser.initialize();
        mMeshProcesser.loadMesh(objF.verticesBuffer, objF.facesBuffer);
        int nV = mMeshProcesser.numV();
        int nF = mMeshProcesser.numF();
    }
    /*Set distance EyeZ*/
    public void setDistanceEyeZ(float scale)
    {
        distanceEyeZ = distanceEyeZ * scale;
    }
    /*Rotate model*/
    public void rotateObject(float x, float y, boolean ifFirstTouch)
    {
        if(ifFirstTouch == true)
        {
            xdiff = x - rotationAngleY;
            ydiff = -y + rotationAngleX;
        }else
        {
            rotationAngleY = x - xdiff;
            rotationAngleX = y + ydiff;
        }
    }
    /*Move eye's position*/
    public void moveEye(float x, float y)
    {
        moveX += x;
        moveY += y;
    }
    /*Reset user's view*/
    public void resetView()
    {
        moveX = 0.0f;
        moveY = 0.0f;
        distanceEyeZ = -4;
    }
    /*Harmonic*/
    public void runHarmonic()
    {
        ifHarmonic = true;
    }
}