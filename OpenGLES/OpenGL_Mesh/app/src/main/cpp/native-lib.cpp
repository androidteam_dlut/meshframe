#include <jni.h>
#include <string>
#include <MeshLib/core/Mesh/BaseMesh.h>
#include <MeshLib/core/Mesh/Vertex.h>
#include <MeshLib/core/Mesh/HalfEdge.h>
#include <MeshLib/core/Mesh/Edge.h>
#include <MeshLib/core/Mesh/Face.h>
#include <MeshCodes/HarmonicMap/HarmonicMap.h>


using namespace MeshLib;

class CHEHm : public CHalfEdge, public _halfEdgeHarmonic {};
class CEdgeHm : public CEdge, public _edgeHarmonic {};
class CVertexHm : public CVertex, public _vertexHarmonic {};
typedef CBaseMesh<CVertexHm, CEdgeHm, CFace, CHEHm> M;


struct MeshData{
    jshort * pFacesBuf;
    jfloat * pPointsBuf;
    jint numV;
    jint numF;

    M* pM;
    HarmonicMap<M>* pHMap;
};

extern "C"
JNIEXPORT jstring JNICALL
Java_anka_opengl_1jni_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT jlong JNICALL
Java_anka_opengl_1jni_MeshProcesser_n_1initialize(JNIEnv *env, jobject instance) {

    MeshData * pMeshData = new MeshData;
    return  (jlong)pMeshData;

}

extern "C"
JNIEXPORT jboolean JNICALL
Java_anka_opengl_1jni_MeshProcesser_n_1loadMesh(JNIEnv *env, jobject instance, jlong p32MeshDataAdr,
                                                jobject points, jobject faces)
{
    MeshData *pMeshData = (MeshData *) p32MeshDataAdr;
    pMeshData->pPointsBuf = (jfloat *) env->GetDirectBufferAddress(points);
    pMeshData->numV = (jint)env->GetDirectBufferCapacity(points) / 3;
    pMeshData->pFacesBuf = (jshort *) env->GetDirectBufferAddress(faces);
    pMeshData->numF = (jint)env->GetDirectBufferCapacity(faces) / 3;
    pMeshData->pM   = new M;

    pMeshData->pM->read_buffer(pMeshData->pPointsBuf, pMeshData->numV, pMeshData->pFacesBuf, pMeshData->numF);

    pMeshData->pHMap = new HarmonicMap<M>;
    pMeshData->pHMap->setInputMesh( pMeshData->pM);
    return (jboolean) true;
}

extern "C"
JNIEXPORT jlong JNICALL
Java_anka_opengl_1jni_MeshProcesser_n_1numV(JNIEnv *env, jobject instance, jlong p32MeshDataAdr) {

    MeshData * pMeshData = (MeshData *) p32MeshDataAdr;

    return  pMeshData->numV;
}

extern "C"
JNIEXPORT jlong JNICALL
Java_anka_opengl_1jni_MeshProcesser_n_1numF(JNIEnv *env, jobject instance, jlong p32MeshDataAdr) {

    MeshData * pMeshData = (MeshData *) p32MeshDataAdr;

    return  pMeshData->numF;

}

extern "C"
JNIEXPORT void JNICALL
Java_anka_opengl_1jni_MeshProcesser_n_1shrinkBy(JNIEnv *env, jobject instance, jlong p32MeshDataAdr,
                                                jfloat level) {
    MeshData * pMeshData = (MeshData *) p32MeshDataAdr;
    static bool succeed = false;
    static bool started = false;

    static  int count = 0;

    if (count == 150){
        pMeshData->pHMap->setInitalMap();
        started = true;
    }

    ++count;
    if (!succeed && started){
        succeed = pMeshData->pHMap->adjustPointVisualOneStep();
    }
    pMeshData->pM->pullBackPointsToBuf();
}